let canvasHeight = 1000;
let canvasWidth = 1600;
let fontSize = 24;

// Headlines
let ignorance = "The Real World";
let sympathy = "The Ideal World";

// You are enough lines
let compassion = "You are enough";
let humanity = "You will always be enough";

// If the Ideal world should be drawn
let theWorldWasNice = false;

// ignorantComment is the first text on the canvas
var ignorantComment = null;
// transphobicComment is the second text on the canvas
var transphobicComment = null;

// assignIndex is used to keep track of which text should be assigned to ignorantComment/transphobicComment
var assignIndex = 0;
// assignAt is used to keep track of when an assignment should happend
var assignAt = new Date();
// assignTo is used to track if assignment should be to ignorantComment (1) or transphobicComment (2)
var assignTo = 1;

// Statements is an array and will be populated with texts from json file
var statements = [];

// preload data before setup
function preload() {
  soundFormats('mp3');
  whiteNoise = loadSound('whitenoise.mp3');
  // load the fonts
  oswaldBold = loadFont("Oswald-SemiBold.ttf");
  oswaldLight = loadFont("Oswald-Light.ttf");
  // load the statements
  loadJSON("statements.json", function (data) {
    statements = data.statements;
  });
}

// setup the canvas
function setup() {
  createCanvas(canvasWidth, canvasHeight);
  whiteNoise.play();
}

function draw() {
  // Draw background
  background(0);

  // If theWorldWasNice is true, then we draw the ideal world headline and texts, and return to avoid running the normal draw logic
  if (theWorldWasNice) {
    showTheIdealWorld();
    return;
  } else {
    showTheRealWorld();
  }
}

// Draw a statement with text, x and y cords, and width
function display(statement, x, y, width) {
  textFont(oswaldLight); // Use OswaldLight font
  textSize(fontSize); // Set the text size
  textAlign(CENTER, TOP); // Center X, TOP Y
  fill(255, 255, 255); // White color
  text(statement, x, y, width); // Draw text
}

// If the space key (32) is pressed, flip the drawIdealWorld boolean
function keyPressed() {
  if (keyCode === 32) {
    theWorldWasNice = !theWorldWasNice;
  }

  if (theWorldWasNice) {
    whiteNoise.stop();
  } else {
    whiteNoise.play();
  }

}

function showTheIdealWorld() {
  // Font properties
  textFont(oswaldLight);
  textSize(100);
  textAlign(CENTER, TOP);
  fill(255, 255, 255);

  // Draw headline
  text(sympathy, 0, 0, 1600, 1000);

  fill('rgba(255, 255, 255, 0.25)')
  textSize(24);
  text("Press Space to add noise", 0, 900, 1600, 300);


  if (new Date().getSeconds() % 2 === 0) {
    // Is seconds dividable with 2, then draw texts in order. If not, draw texts in reverse order
    display(compassion, 200, 300, 1200);
    display(humanity, 200, 500, 1200);
  } else {
    display(humanity, 200, 300, 1200);
    display(compassion, 200, 500, 1200);
  }
}

function showTheRealWorld() {
  // Font properties
  textFont(oswaldLight);
  textSize(100);
  textAlign(CENTER, TOP);
  fill(255, 255, 255);

  // Draw headline
  text(ignorance, 0, 0, 1600, 1000);

  fill('rgba(255, 255, 255, 0.25)')
  textSize(24);
  text("Press Space to remove noise", 0, 900, 1600, 300);

  // If ignorantComment has been assigned a text, draw it
  if (ignorantComment) {
    display(ignorantComment, 200, 300, 1200);
  }

  // if transphobicComment has been assigned a text, draw it
  if (transphobicComment) {
    display(transphobicComment, 200, 500, 1200);
  }

  // If the current time is greater than the assignAt timestamp, assign a text
  var rotateIgnorance = new Date() > assignAt;
  if (rotateIgnorance) {
    // set the new assignAt +4 seconds into future
    d = new Date();
    d.setSeconds(d.getSeconds() + 4);
    assignAt = d;

    // get the text from the statements array
    var statementText = statements[assignIndex];
    // increment the assignIndex
    assignIndex++;
    if (assignIndex > statements.length - 1) {
      // if the assignIndex surpasses the number of elements in the array, reset the assignIndex
      assignIndex = 0;
    }

    // If assignTo is 1, then assign to text 1, and change assignTo to 2
    if (assignTo === 1) {
      // Draw statement
      ignorantComment = statementText;
      assignTo = 2;
    } else if (assignTo === 2) {
      // If assignTo is 2, then assign to text 2, and change assignTo to 1
      transphobicComment = statementText;
      assignTo = 1;
    }
  }
}
