var words
var load=0
var index
let myFont

function preload(){
win=createImg("SIMS.gif")

words=['......... Creating cheatcodes',
'Preparing for motherlode',
'Its not OK to burn your simmers alive.',
'Finding secret hideouts',
'Preparing for cheats',
'Deleting your dirty ass mods',
'Abducting alien babies',
'Blurring simmers',
'Loading the Loading message...',
'Loading "Vroom" Sounds',
'Googling WooHoo'

]

myFont = loadFont('SimsLLHP.ttf');


}

function setup(){

frameRate(7)

createCanvas (600,600);
background(color="#00DE16")

}

function draw() {

//frame

push()
noStroke()
fill("black")
rect(18,198,566,206)
pop()

push()
noStroke()
fill(color="#808080")
rect(18,198,564,204)
pop()

push()
noStroke()
fill("white")
rect(18,198,562,202)
pop()

push()
noStroke()
fill(color="#c0c0c0")
rect(20,200,560,200)
pop()

//banner

push()
noStroke()
fill(color="#00870D")
rect(22,202,556,36)
pop()



//loading bar frame

push()
noStroke()
fill("white")
rect(28,366,544,24)
pop()


push()
noStroke()
fill(color="#04610D")
rect(28,366,542,22)
pop()

push()
noStroke()
fill(color="#c0c0c0")
rect(30,368,540,20)
pop()

// load

for (let i = 0; i<load; i++){

loading(30+15*i);//speed and space in the squares

}


if (load<36){
    load++
} else {
load=0
index=random(words)
}


//text

push()
fill("black");
textFont(myFont);
textSize(20);
text(index, 40, 350);
pop()

push()
fill("white");
textFont(myFont);
textSize(16);
text('SIMS 1', 30, 228);
pop()

//lets get that gif in place
push()
win.size(100,100)
win.position(width*0.5-40,250)
pop()


}

function loading(x){

  push()
  noStroke()
  fill(color="#000080")
  rect(x,370,12,16)
  pop()

}
