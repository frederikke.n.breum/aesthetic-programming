My minix on designing a throbber. --> [sims loading screen](https://frederikke.n.breum.gitlab.io/aesthetic-programming/minix3
)

I saw this other student trying to make a loading screen in windows theme and wanted to try it myself as I thought it was both cool and fun. I couldn't make it that detailed but kinda nailed the old windows look with help from Jacobs code. (Inspired from JacobØstergaard) I wanted to create a old sims 1 inspired loading page. I could make a modern and up to date to the new Sims game but it was way more nostalgic to create this old windows looking one. When reading about throbbers and loops Sims was actually the first thing that came to my mind. I was very young when first introduced to the Sims. I remember that even as a kid the throbber was designed so well even a little kid understood the game wasn't ready and you hads to wait. 
The thing I like about throbbers is when they are created in a creative and fun way. Like The Sims when the loading screen gives you funny startup strings. I could be nice to know about the game or small jokes. 
I tried creating my own ones in this project. 

I like the old fashion design of my program because of the nostalgic part. I made the loading loop work with following code in line 98-110. To be fair I godt to work from looking at others work and my playing around with the code. This is something I want dive more into so I can truly understand it. 


![](simsscreen.png)
