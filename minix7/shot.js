// The Shot class
class Shot {
  // The shot constructor
  constructor(spos, angle) {
    this.position = createVector(spos.x, spos.y);
    this.vel = p5.Vector.fromAngle(angle);
    this.vel.mult(10);
  }

  // update method
  update() {
    this.position.add(this.vel);
  }

  // draw method draws the shot object
  draw() {
    push();
    stroke(0, 106, 205);
    strokeWeight(4);
    point(this.position.x, this.position.y);
    pop();
  }

  // hits method checks if the shot hits the supplied virus
  hits(virus) {
    var d = dist(this.position.x, this.position.y, virus.position.x, virus.position.y);
    if (d < virus.width) {
      return true;
    } else {
      return false;
    }
  }

  // offscreen method checks if the shot if off screen
  offscreen() {
    if (this.position.x > canvasWidth || this.position.x < 0) {
      return true;
    }
    if (this.position.y > height || this.position.y < 0) {
      return true;
    }
    return false;
  }
}
