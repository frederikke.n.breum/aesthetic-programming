// Virus class
class Virus {
  // Virus constructor
  constructor(position, width, rotation) {
    // If position is in constructor, use it. Otherwise set a new random position
    if (position) {
      this.position = position.copy();
    } else {
      this.position = createVector(random(canvasWidth), random(canvasHeight));
    }

    // if the constructor width is defined, set the width or create a random width
    if (width) {
      this.width = width;
    } else {
      this.width = random(30, 100);
    }

    // if the rotation constructor is defined, set the rotation, otherwise set it to random rotation
    if (rotation) {
      this.rotation = rotation;
    } else {
      this.rotation = random(360);
    }

    this.vel = p5.Vector.random2D();
    this.total = floor(random(5, 15));
    this.offset = [];
    for (var i = 0; i < this.total; i++) {
      this.offset[i] = random(-this.width * 0.5, this.width * 0.5);
    }
  }

  // update method adds the velocity vector to the position each draw cycle
  update() {
    this.position.add(this.vel);
  }

  // draw method
  draw() {
    push();
    translate(this.position.x, this.position.y);
    rotate(this.rotation + PI / 2);
    image(virus, -(this.width / 2), -(this.width / 2), this.width, this.width);
    pop();
  }

  // breakup method will break the virus up into smaller viruses, if it is not already too small
  breakup() {
    var newViruses = [];

    if (this.width > 25) {
      newViruses[0] = new Virus(this.position, this.width / 2, null);
      newViruses[1] = new Virus(this.position, this.width / 2, null);
    }

    return newViruses;
  }

  // checkEdges method checks if the virus exceeds the canvas borders, and moves it to the opposite side
  checkEdges() {
    if (this.position.x > canvasWidth + this.width) {
      this.position.x = -this.width;
    } else if (this.position.x < -this.width) {
      this.position.x = canvasWidth + this.width;
    }
    if (this.position.y > canvasHeight + this.width) {
      this.position.y = -this.width;
    } else if (this.position.y < -this.width) {
      this.position.y = canvasHeight + this.width;
    }
  }
}
