// Inspired by https://editor.p5js.org/fvzulueta94/sketches/H1GaVSr3f
let canvasHeight = 800;
let canvasWidth = 1200;

// Game objects
var syringe = null;
var viruses = [];
var shots = [];

// preload to load the images before setup
function preload() {
  syringeImg = loadImage("syringe.png");
  virus = loadImage("virus.png");
}

// setup the canvas and create the ship
function setup() {
  backgroundColor = color(0);
  createCanvas(canvasWidth, canvasHeight);

  // Create the syringe, at the center of the canvas
  syringe = new Syringe(createVector(canvasWidth / 2, canvasHeight / 2));

  // Create 10 virus objects
  for (var i = 0; i < 10; i++) {
    viruses.push(new Virus());
  }
}

function draw() {
  // If Up arrow key pressed, then boost the syringe
  if (keyIsDown(UP_ARROW)) {
    syringe.boost();
  }

  // If right or left key is pressed, rotate the syringe
  if (keyIsDown(RIGHT_ARROW)) {
    syringe.turn(0.1);
  } else if (keyIsDown(LEFT_ARROW)) {
    syringe.turn(-0.1);
  }

  // Draw background
  background(backgroundColor);

  // Virus logic and draw
  for (var i = 0; i < viruses.length; i++) {
    viruses[i].draw();
    viruses[i].update();
    viruses[i].checkEdges();
  }

  // Shots logic and draw
  for (var i = shots.length - 1; i >= 0; i--) {
    shots[i].draw();
    shots[i].update();
    if (shots[i].offscreen()) {
      // If shot if off screen, remove it
      shots.splice(i, 1);
    } else {
      for (var j = viruses.length - 1; j >= 0; j--) {
        if (shots[i].hits(viruses[j])) {
          // If shot hits virus, break it up
          var newViruses = viruses[j].breakup();
          viruses = viruses.concat(newViruses);
          viruses.splice(j, 1);
          shots.splice(i, 1); // Remove shot
          break;
        }
      }
    }
  }

  // Syringe logic and draw
  syringe.update();
  syringe.draw();
  syringe.checkEdges();
}

function keyPressed() {
    // If space key is pressed, add new shot
  if (key == " ") {
    shots.push(new Shot(syringe.position, syringe.heading));
  }
}