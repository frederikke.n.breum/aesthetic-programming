**Asteroids x 2021**

[The game](https://frederikke.n.breum.gitlab.io/aesthetic-programming/minix7)
![](rona.png)



**How does/do your game/game objects work and describe how you program the objects and their related attributes, and the methods in your game.**

**Syringe**
The syringe object is the player. Only one of this object is initialized.
The syringe is controlled by the player, and has movement behaviour based on user input

**Virus**
Multiple virus objects are created when the game starts.
The virus objects are moving around on the canvas, and can be destroyed by shots.
When a shot destroys a virus, it will multiply into two new virus objects.
If small enough, it will not multiply.

**Shot**
The shot objects are created when the user pressed the space button.
Shots will be shot out from the front of the syringe, and have the same heading as the syringe.
When a shot hits a virus, the virus will break up (and possible multiply)

The program contains 3 different classes
The Syringe,
The Virus,
The Shot.
Each class has its own attributes. The attributes describe the state of the object. For example the position, the rotation, the size etc.
Each class also has methods. Those methods can mutate the state (attributes) of the object, for example change the position of the object.
I have tried to encapsulate the behavior (logic) of each class in its methods, and the state of the object in its attributes. 

**Draw upon the assigned reading, are the characteristics of object-oriented programming and the wider implications of abstraction?**
Object Oriented Programming is a programming paradigm that focuses on “objects”.
Objects contain data in the form of attributes (or properties), and methods (logic or code) that typically contains behaviour for the object.
In large and complex applications, OOP can be very beneficial to improve the structure and maintainability of the application.
The abstraction in OOP causes the implementation itself to be hidden, and the focus is on the “interface” of the object. You as the “user” of an object does not need to concern yourself with the inner workings of the implementation. You are only required to supply the arguments for the public constructor/methods/properties/attributes, and the internal implementations will be hidden from you.

An example I found very useful to understand OOP:

"_If you've ever made coffee using a coffee machine, you have probably experience abstraction. When you make your coffee, you make sure that there are ample coffee beans, you add water, add milk, and perhaps put in a fresh coffee filter. Then, you hit a button and a coffee of your choice is made. This is a good example of abstraction since the user doesn't need to be concerned about the right amount of milk needed for a specific type of coffee, or how many beans need to be ground up for the right amount, or what the temperature of the water needs to be, the user is only considered with providing the necessary materials to produce the final product. This same structure applies to object oriented programming. The user of the object should only be concerned with providing the necessary parameters for the final product and shouldn't really know or need to know how the object works internally._"

**Connect your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?**
The game is a old game asociated with the old game archades where young people and kids met to hang out and play games. I made the game with a 2020/2021 touch. Instead of the ship and the meteor I made the Covid virus as the meteor and the vaccine as the ship there is supposed to destroy the rocks, but not the virus. I made the game so the Virus cant win.. So its basically the vaccine which can destroy the virus. You need a little win these days where it feels like the virus have won for many months. 
**An example for abscraction related to oop:** Shot.hits method returns true or false if the shot hits a virus object. You as the “user” of the object method do not know the inner workings of that method. But you know what it will return true or false if the shot hits the virus supplied as the method argument. An overall great example is the coffe maker example as above. We dont know how the coffee is made we just know we will get a cup of coffee. 

**References:**
Inspired by https://editor.p5js.org/fvzulueta94/sketches/H1GaVSr3f
https://en.wikibooks.org/wiki/Object_Oriented_Programming/Abstraction
