// Syringe class
class Syringe {
  // Syringe constructor
  constructor(position) {
    this.position = position;

    this.width = 20;
    this.length = 40;

    this.heading = 0;
    this.velocity = createVector(0, 0);
    this.isBoosting = false;
  }

  // update method
  update() {
    // Add the velocity vector to make the syringe move
    this.position.add(this.velocity);

    // Decrease the velocity vector a little
    this.velocity.mult(0.99);
  }

  // turn method
  turn(rotation) {
    this.heading += rotation;
  }

  // Boost method adds force to the velocity vector
  boost() {
    var force = p5.Vector.fromAngle(this.heading); // x = 1, y = 0
    force.mult(0.1);
    this.velocity.add(force);
  }

  // Check edges methods, checks if the syrringe is out of bounds, if so, then move on the oppesite side.
  checkEdges() {
    if (this.position.x > canvasWidth + this.width) {
      this.position.x = -this.width;
    } else if (this.position.x < -this.width) {
      this.position.x = canvasWidth + this.width;
    }
    if (this.position.y > canvasHeight + this.length) {
      this.position.y = -this.length;
    } else if (this.position.y < -this.length) {
      this.position.y = canvasHeight + this.length;
    }
  }

  // Draw method draw the syrringe on the canvas
  draw() {
    // Save current translate settings
    push();
    // Start translate based on syrringe position
    translate(this.position.x, this.position.y);
    // rotate based on the heading
    rotate(this.heading + PI / 2);
    // draw the syrringe, but offset the image so that the x,y point is in the middle of the image, otherwise the rotation is around the images 0,0 point
    image(
      syringeImg,
      -(this.width / 2),
      -(this.length / 2),
      this.width,
      this.length
    );

    // Restore the settings from the push
    pop();
  }
}
