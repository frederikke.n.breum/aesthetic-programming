function setup() {
  var c =createCanvas(1400,1400);
  scale(3);

  noStroke();
  fill(255, 20, 100)
  ellipse(200,100,400,400);


  //notcreepyemoji
  fill(255,204,0);
  noStroke();
  ellipse(75,75,50,50);
  fill(255,154,227);
  ellipse(85,55,10,10);
  ellipse(94,55,15,12);
  ellipse(78,55,15,12);
  //eyes
  fill(255,255,255);
  ellipse(66,70,10,14);
  ellipse(84,70,10,14);
  fill(115,159,201);
  ellipse(66,72,8,8);
  ellipse(84,72,8,8);
  //mouth
  noFill();
  stroke(204,102,0);
  arc(75, 75.8, 200, 26, 	QUARTER_PI,PI-QUARTER_PI);


  //Creepyemoji
  fill(255,204,0);
  noStroke();
  ellipse(275,75,50,50);
  //eyes
  fill(204,102,0)
  ellipse(265,70,4,2);
  ellipse(280,70,4,2);
  ellipse(280,62,10,2);
  ellipse(265,64,10,2);

  //mouth
  noFill();
  stroke(204,102,0);
  strokeWeight(2);
 arc(275, 80, 30, 22,0,QUARTER_PI*2);
}
