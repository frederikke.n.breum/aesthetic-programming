![](masterpiece2.png)

[Link](https://frederikke.n.breum.gitlab.io/aesthetic-programming/Minix2
)

So this masterpiece is called "a typical night out before the big C" 
Lacking nights out was my biggest inspiration for this. After remembering the creepy dudes at a night out I dont miss it as much. Win. 

I created this without the use of variables as I didn't see the need. I tried making eyelashes on the girl with use of "line" but I couldn't get the coordinates right. I struggled as well with curving lines. I made a big ellipse to illustrate the lights when you are at a club. I thought of making it blink when the mouse is pressed. But I couldn't make it function the way i wanted to so I just made it a bright color. 
I learned to use the Scale()function wich is pretty nice. I needed to read about it a few times before getting it. 
My Emoji is portraiting the typical social sterotypes when Men and Woman are going out and drinking. It is typical Men who are portraited as creepy and nasty and the woman who would rather be free of the attention the Men are providing. 
