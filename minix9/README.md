[Cats](https://frederikke.n.breum.gitlab.io/aesthetic-programming/minix9)
![](lillekat.png)


What is the program about? Which API have you used and why?

The program is a random cat facts generator. We used APIs to give me random facts and random cat pictures. The last weeks the Minixs have been very serious and political so this time we just wanted to make something funny. 

Can you describe and reflect on your process in this miniX in terms of acquiring,
processing, using, and representing data? 
The application loads a random cat image and a random cat fact and presents this data by rendering the cat image as the backgroound and drawing the random cat fact in top of it. 

How much do you understand this data or what do you want to know more about? 
This data is very simple. 
One of the endpoints returns image data and the other one return a simple JSON object with a `fact` property containing a random cat fact. 


How do platform providers sort the data and give you the requested data?
These APIs are very simple and I asssume that the provider just return a random cat fact or image from their database. 


What are the power relations in the chosen APIs? What is the significance of APIs in digital culture?


Try to formulate a question in relation to web APIs or querying/parsing processes that you would like to investigate further if you had more time.
I would like to investigate and explore REST(ful) APIs. 
