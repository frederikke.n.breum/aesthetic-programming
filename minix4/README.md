Minix4 - BEAUTY IN 2021 

A very simple demonstration of what may be a very big part of the lives of mainly girls. 
My code is very simple syntax but it is basically a function many of girls use every day. Beautyfilter, filter and so on. It has become an everyday thing to edit pictures or take pictures with these called 'beauty filters' 
This captures the time when you push the 'make me pretty' button. A button that is not there in real life, but the cognitive part of the button is. We use filters as blur and tint to make ourself look and feel prettier. 
This button may not be there in real life but when just using the filter to take a picture the same thing is happening. The action is captured and saved but in a not so obvious way. 
The tint is used to make it look like 'golden hour' which is the big thing in 2021.

(please use in chrome browser)

[link](https://frederikke.n.breum.gitlab.io/aesthetic-programming/minix4)

 
![](goldenhour.png)
