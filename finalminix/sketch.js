let canvasHeight = 1000;
let canvasWidth = 1600;
let fontSize = 24;

let ignorance = "The Real World";
let sympathy = "The Ideal World";

let compassion = "You are enough";
let humanity = "You will always be enough";

let theWorldWasNice = false;

var ignorantComment = null;
var transphobicComment = null;

var assignIndex = 0;
var assignAt = new Date();
var assignTo = 1;

var statements = [];

function preload() {
  soundFormats('mp3');
  whiteNoise = loadSound('whitenoise.mp3');
  // load the fonts
  oswaldBold = loadFont("Oswald-SemiBold.ttf");
  oswaldLight = loadFont("Oswald-Light.ttf");
  // load the statements
  loadJSON("statements.json", function (data) {
    statements = data.statements;
  });
}

// setup the canvas
function setup() {
  createCanvas(canvasWidth, canvasHeight);
  whiteNoise.play();
}

function draw() {
  // Draw background
  background(0);

  if (theWorldWasNice) {
    showTheIdealWorld();
    return;
  } else {
    showTheRealWorld();
  }
}

function display(statement, x, y, width) {
  textFont(oswaldLight); // Use OswaldLight font
  textSize(fontSize); // Set the text size
  textAlign(CENTER, TOP); // Center X, TOP Y
  fill(255, 255, 255); // White color
  text(statement, x, y, width); // Draw text
}

function keyPressed() {
  if (keyCode === 32) {
    theWorldWasNice = !theWorldWasNice;
  }

  if (theWorldWasNice) {
    whiteNoise.stop();
  } else {
    whiteNoise.play();
  }

}

function showTheIdealWorld() {
  textFont(oswaldLight);
  textSize(100);
  textAlign(CENTER, TOP);
  fill(255, 255, 255);

  text(sympathy, 0, 0, 1600, 1000);

  fill('rgba(255, 255, 255, 0.25)')
  textSize(24);
  text("Press Space to add noise", 0, 900, 1600, 300);


  if (new Date().getSeconds() % 2 === 0) {
    display(compassion, 200, 300, 1200);
    display(humanity, 200, 500, 1200);
  } else {
    display(humanity, 200, 300, 1200);
    display(compassion, 200, 500, 1200);
  }
}

function showTheRealWorld() {
  textFont(oswaldLight);
  textSize(100);
  textAlign(CENTER, TOP);
  fill(255, 255, 255);

  text(ignorance, 0, 0, 1600, 1000);

  fill('rgba(255, 255, 255, 0.25)')
  textSize(24);
  text("Press Space to remove noise", 0, 900, 1600, 300);

  if (ignorantComment) {
    display(ignorantComment, 200, 300, 1200);
  }

  if (transphobicComment) {
    display(transphobicComment, 200, 500, 1200);
  }

  var rotateIgnorance = new Date() > assignAt;
  if (rotateIgnorance) {
    d = new Date();
    d.setSeconds(d.getSeconds() + 4);
    assignAt = d;

    var statementText = statements[assignIndex];
    assignIndex++;
    if (assignIndex > statements.length - 1) {
      assignIndex = 0;
    }

    if (assignTo === 1) {
      ignorantComment = statementText;
      assignTo = 2;
    } else if (assignTo === 2) {
      transphobicComment = statementText;
      assignTo = 1;
    }
  }
}
