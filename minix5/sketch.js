let spacing = 40;
let canvasWidth = 1000;
let canvasHeight = 640;

// the lines array, will contain all the line objects created
var lines = [];

function setup() {
  createCanvas(canvasWidth, canvasHeight);
  background("#1F2832");
  frameRate(10);
    // Create the lines that fit on the canvas
    var x = 0;
    var y = 0;
    var i = 0;
    while (1) {
      // If we exceed canvas width, reset x and change to next "line"
      if (x >= canvasWidth) {
        x = 0;
        y = y + spacing;
      }

      // If y exceeds or is equal to canvas height, stop adding lines
      if (y >= canvasHeight) {
        break;
      }

      // Create new stroke and draw it
      lines.push(new Line(x, y, x+spacing, y+spacing))
      x = x + spacing;
      i++;
    }
}

function draw() {
  background("#1F2832");

  // Iterate all lines in the lines array
  for (var line of lines) {
    // Create random number betwenn 0-1
    rand = random(1);

    // If greater than 0.5, rotate the line
    if (rand > 0.5) {
      line.rotate();
    }

    // If less than or equal to 0.5, set the line color to a random color
    if (rand <= 0.5) {
      line.randomColor();
    }

    // Draw the line
    line.draw();
  }
}

// Line function create a new line object
function Line(x1, y1, x2, y2) {
  this.x1 = x1;
  this.y1 = y1;
  this.x2 = x2
  this.y2 = y2
  this.r = 102
  this.g = 252;
  this.b = 241;
  this.rotated = false;

  // randomColor sets the lines rgb values to a random number between 0-255
  this.randomColor = function() {
    this.r = random(255)
    this.g = random(255)
    this.b = random(255)
  }

  // rotate set the line to rotated or not
  this.rotate = function() {
    this.rotated = !this.rotated
  }

  // the draw function will draw the line
  this.draw = function() {
    stroke(this.r, this.g, this.b);
    strokeWeight(5);

    if (this.rotated) { // If rotated, draw the line rotated (swap x1 and x2)
      line(this.x2, this.y1, this.x1, this.y2)
    } else { // if not rotated, just draw the line as normal
      line(this.x1, this.y1, this.x2, this.y2)
    }
  }
}