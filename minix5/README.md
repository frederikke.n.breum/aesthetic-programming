Minix5

[Link ](https://frederikke.n.breum.gitlab.io/aesthetic-programming/minix5
)to project

10PPRINT disco party

![](10print.png)

Reference: Daniel Shiffman [10PRINT](https://www.youtube.com/watch?v=bEyTZ5ZZxZs)
I made the 10point system from the video more advanced and colorful to make it my own. 

In my for-loop I adjusted it to be a more simple code but the function is the same as if I used the typical for loop or while loop code. 
During setup, I create all the line objects, that fit on the canvas. There cannot be drawn more lines, than the canvas can contain. The canvas width and height is used to calculate when to stop creating lines. The lines are added to an array.
Each draw cycle in the canvas is redrawn, the lines array is iterated, and all lines are drawn again.
The script is using if conditions and randomness. Each line has a 50% chance to be drawn rotated, or a 50% change to get a new random color.

There is a very low level of control and autonomy because its mostly randomness. 
To make a generator with high level of control and autonomy I could for an example create a 'snake' like game where the snake needed to avoid curtian shapes. 
It operates through for an example, probability, which is made by me through variables and is for me to change any time and be in control of the "random" generator. So you can say my auto-generator is creating randomness to a point but im still the decider and creator of the dimensions of randomness. 
