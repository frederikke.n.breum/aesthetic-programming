var sunX = 100;
var sunY = 600;
let maxSunY = 600;
let sunGoingUp = true;

function setup() {
  frameRate(30);
  // Set to and from colors to use based on the day cycle
  sunColorFrom = color(255, 247, 58);
  sunColorTo = color(255, 255, 255);
  backgroundColorFrom = color(135, 206, 235);
  backgroundColorTo = color(7, 11, 50);
  windowColorFrom = color(0, 0, 0);
  windowColorTo = color(255, 247, 58);
  createCanvas(600, 600);
}

function draw() {
  // Background
  backgroundColor = lerpColor(
    backgroundColorFrom,
    backgroundColorTo,
    sunY / 600
  );
  background(backgroundColor);

  // Random stars if sun Y is more than or equal to 400 (the sun is lowest)
  if (sunY >= 400) {
    for (i = 0; i < -(400 - sunY); i++) {
      fill(255, 255, 255);
      ellipse(random(0, 600), random(0, random(0, 600)), 2, 2);
    }
  }

  // Move the sun if mouse is pressed
  if (mouseIsPressed) {
    if (sunY <= 0) {
      sunGoingUp = false;
    }

    if (sunY >= maxSunY) {
      sunGoingUp = true;
    }

    // If the sun is going up, we decrease the Y value to make the sun rise, otherwise increase the Y value to make the sun go down
    if (sunGoingUp) {
      sunY = sunY - 10;
    } else {
      sunY = sunY + 10;
    }
  }

  // House
  fill(121, 0, 0);
  rect(300, 400, 200, 200);

  // Roof
  fill(9, 20, 121);
  triangle(300, 400, 400, 250, 500, 400);

  // Window
  windowColor = lerpColor(windowColorFrom, windowColorTo, sunY / 600); // change color based on sun Y value
  fill(windowColor);
  rect(350, 450, 50, 50);

  // Sun
  sunColor = lerpColor(sunColorFrom, sunColorTo, sunY / 600);
  fill(sunColor);
  noStroke();
  ellipse(sunX, sunY, 100, 100);
}
