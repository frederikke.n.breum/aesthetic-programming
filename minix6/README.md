[Link ](https://frederikke.n.breum.gitlab.io/aesthetic-programming/minix6
)to project 


![](hus2.png)
![](hus3.png)

Which MiniX do you plan to rework? I plan to work on my first minix to spice it up. It was very plain and I think it could be fun to make it way more advanced and look back at my work and see of much more I would be able to make now. 

What have you changed and why?
I have changed the things I wanted to make the first time but couldnt manage to. Now its easier to find code (The coding train all the way) and fully understand it. The P5 libary is way more simple to search in and understand now. So I decided I wanted to make my kidnergarten like drawing more dynamic and basicly I wanted to change the image from day to night. The lerpColor from p5.js made the job even better to make the colors fade. 

How would you demonstrate aesthetic programming in your work?
By making the work more advanded I changed the image from being something you would associate with a child static picture with a more dynamic picture. The way you perceieve the picture is changed by some extra lines of code. 

What does it mean by programming as a practice, or even as a method for design?
Programming will in the beginning be a lot of nonsens and new syntax but it really is a powerful tool to visualize and create art and design in general. It can be more than code and art. It can be political and social statements as discovred in minix2 with the meaning of emojis. 

What is the relation between programming and digital culture?
The programming part of this are making a drawing that most of us can recognize from our childhood. The code make us think of something specific as childhood and simpler times. So basicly a few codes of line can make the most of us think about the same memory. 

Can you draw and link some of the concepts in the text (Aesthetic Programming/Critical Making) and expand on how does your work demonstrate the perspective of critical-aesthetics? 
I didnt start this project with a specific decided path. I wasn't reflective in the process. I wanted to explore the p5.js and wanted to create something easy. I continued this way of thinking as why there are no reflection or messages. I just wanted to make something fun a little more advanced. 
