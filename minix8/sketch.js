let canvasHeight = 1000;
let canvasWidth = 1600;
let fontSize = 36;

// statements array to hold all our statements after they are initialized in setup
var statements = [];

// preload data before setup
function preload() {
  // load the font
  reeniefont = loadFont("ReenieBeanie-Regular.ttf");
  // load the statements
  jsonData = loadJSON("statements.json");
}

// setup the canvas
function setup() {
  createCanvas(canvasWidth, canvasHeight);

  // Setup colors
  blackColor = color(0, 0, 0);
  whiteColor = color(255, 255, 255);

  // Iterate the loaded statement in JSON, and create new Statement objects for each
  for (jsonStatement of jsonData.statements) {
    // Create statement class object from json object
    var statementObject = new Statement(
      jsonStatement.x,
      jsonStatement.y,
      jsonStatement.width,
      jsonStatement.height,
      jsonStatement.text
    );
    // Add the newly created Statement object to the array of statement objects
    statements.push(statementObject);
  }
}

function draw() {
  // Draw background
  background(0);

  // Draw headline
  textFont(reeniefont);
  textSize(100);
  textAlign(CENTER, CENTER);
  fill(whiteColor);
  text("They are just kids", 0, 0, 1600, 80);

  // Draw statements
  for (statement of statements) {
    // Check if mouse x,y is hovering over text. If so increase visibility. If not decrease visibility
    if (statement.isWithinBoundingBox(mouseX, mouseY)) {
      statement.increaseVisibility();
    } else {
      statement.decreaseVisibility();
    }

    // Draw statement
    statement.draw();
  }
}

class Statement {
  constructor(x, y, width, height, text) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.text = text;
    // Visiblity starts out at 0, and can be from 0.0 to 1.0
    this.visibility = 0;
  }

  // draw the statement
  draw() {
    textFont(reeniefont);
    textSize(fontSize);
    textAlign(LEFT, TOP);
    var color = lerpColor(blackColor, whiteColor, this.visibility);
    fill(color);
    text(this.text, this.x, this.y, this.width);
  }

  // Increase visibility factor by 0.01
  increaseVisibility() {
    this.visibility = this.visibility + 0.01;
    if (this.visibility >= 1) {
      this.visibility = 1;
    }
  }

  // Decrease visibility factor by 0.01
  decreaseVisibility() {
    this.visibility = this.visibility - 0.01;
    if (this.visibility <= 0) {
      this.visibility = 0;
    }
  }

  // isWithinBoundingBox checks if the supplied x and y point, is within the statements bounding box
  isWithinBoundingBox(x, y) {
    if (
      x >= this.x &&
      x <= this.x + this.width &&
      y >= this.y &&
      y <= this.y + this.height
    ) {
      return true;
    }

    return false;
  }
}
