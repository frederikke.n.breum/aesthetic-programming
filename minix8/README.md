[The work](https://frederikke.n.breum.gitlab.io/aesthetic-programming/minix8)
![](theyarejustkids.png)

**They are just kids**
Work description:

The work is called ‘They are just kids’ The work is created to highlight some of the most dramatic opinions and statements in the debate about the children who are imprisoned in Syria. The statements will appear and make the ‘they are just kids’ header not so simple and show how people do not agree with that simple and logical statement. Suddenly kids are not just kids.. If they are not danish enough. These are real comments from real people. The statements are captured from Facebook. The comments aren’t alone. There are examples of posts with more than 600 comments where most of them are like the statements in the program.
The work concerns how easy it is for the common danish people sitting at home, behind their screens, to condemn small children around the age of 4, neglect them and even to wish them harm. To these people, they are not children, just because their parents might have made some wrong or even questionable choices. As the children are not 100% danish, whatever that means, and the given situation with their mothers have a way of challenging people’s humanity to a place where empathy and anger have the last say in the matter. It is always far too easy to forget about our fellow human beings and turn our backs to whatever might be the easiest - compassion and empathy.
The work might as well also be called “The best interests of the child < immigration policy”, since these many opinions are a crystal clear image of people who support very tough foreign policy in Denmark, as well as being able to see the child before nationality cannot be set aside. This is an example of how politics can be more important than a small child's upbringing and future.      
The comments from Facebook utilizes it as the main medium. However, the texts are the voices of others. Its opinions about a very ethical matter that are easily displayed for everybody. When writing to a screen instead of saying it to the person’s face, the people behind the comments have shown that they are not afraid to show their lack of empathy.

Syntax:
First of all we created the canvas and chose it to be the size of 1000(H) and 1600(W). Subsequently we addressed a variable for the statements to hold all our statements after they are initialized in the setup. 
We also used the function preload to preload data before setup. The preloaded data was the font-type and the statement data. 

Starting the function setup we both determined the canvas and the colors of the interface. 
Furthermore, in order to iterate the loaded statement in JSON, we had to make a for loop to create “new Statement” objects for each, then we created a statement class object from the JSON object and called it forward using variables and arrays.

In the function draw we chose the background color and adjusted the visual outcome of the text; color, size, font, placement etc. 

In order to make the Facebook statements appear on the canvas when the mouse hovers over and disappear again when mouse is removed, we once again made a for-loop with a conditional statement inside to control the outcome. So if the mouse hovers over “statement.increaseVisibility” otherwise (else) “statement.decreaseVisibility”. 
The p5 'lerpColor' helped with making the fading effect and setting a max on 1 and 0. So the colors cant continue from one. 

We used the class syntax to control the statements and their “behavior” on the interface. The placement on the x and y axis, and how they will emerge from invisibility. 
Subsequently we adjusted the draw of the statement and how they will appear on the interface with text font, size, align etc.

Lastly, we used the “isWithinBoundingBox” with the purpose of checking if the supplied x and y point is within the statements bounding. 

Analysis
As mentioned in the text ‘Vocable Code’ by Soon, Winnie & Cox, Geoff, “coding is like speaking, and can also be understood as peotry, and more precise; poems, because they can operate performatively, as they can be read and ideally spoken aloud” (p. 167, Soon, Winnie & Cox, Geoff). 
There are a lot of similarities between writing a code, and the execution of the code in performance. 
When looking at our work, it is clear that the code is short and precise, but the execution of the work has a lot more to say. By making the background black, and the textcolor white, we are making the statements appear very clear to the user of the program. The white color of the text is therefore used as a way to put these statements in focus. 
Also, in the code you can't read the statements, this is just when the mouse passes the certain points on the x and y axis. This also emphasizes how the execution of the work shows more of the written code, because you kind of “experiences” the code in a deeper level, and sees what's “behind” it. 

With the definition of “vocable code” as “both a work of software art and a “codework”, and something collecting voices and statements” (p. 168, Soon, Winnie & Cox, Geoff), it links to our work exactly because we manage to present statements from the real world that has something to say about a topic, that everyone has an opinion about. 
We also try to “talk to” the user of the program, by showing these statements and also by creating a title that seems a little sarcastic when you read these statements. As mentioned in the description, it seems like such an easy thing for people online, to just shoot out different statements, and even statements about topics that they aren’t that informed about. 
The font is used to look like handwriting as the statements are personal writting statements from real people. It is not just from a machine. The font is making it seem more like a human action rather than a computer action/statement. 

As in “vocable code” the program only selects one text to speak/play at a time, because in our case, the mouse has to be dragged over certain x and y points to present the statements. We chose to present the statements like this to make the individual statements more clear and to put each and everyone of them in focus when they are read. 

We both used visual effects as the colors and the sizes of the text to express these statements and at the same time; the importance of the topic. It will always be up to the user to create their own understanding of the work, but with the use of the black background, the white texts to make them appear clearly on the background, and the importance of showing only one statement at a time, we did everything we could to show how we think this is not okay. 
Sitting at home writing these statements is saying a lot more about the person writing them, than the ones the statements are about. 

Reflection
Just like Winnie Soon’s art work “Vocable Code”, it bears similar critical design. The racial and religious divide in our country has become deeper and deeper in the last couple of years with even tougher foreign policy in Denmark. As mentioned in Soon and Cox’s book; 

“What we experience are the performative qualities of code in terms of both its human and nonhuman execution. When code becomes executable, it blends ‘form and function’, and becomes something which can be read, interpreted, executed and performed. We see the code and we hear contributor’s spoken statements that, together, allow the program to speak to us.” (p.168)

As well as Winnies own work, our work is also about how we can connect, interpret, and so on, the written words of real people from Facebook. We might not realise in our day to day life that there are people with these kinds of inhuman opinions, as we do not experience or share these opinions ourselves. However, that does not mean that some people do share these opinions and might think twice if they knew their comments/statements were to be publicly displayed and judged.      

As mentioned in the analysis, we tried to incorporate our own thoughts in the work, by trying to express the statements in a certain way, but in the end, it will always be up to the user/receiver of the work, to create their own understanding of the work. 
The reason why we chose this exact topic to work on in this minix, is to emphasize how big a problem it is, that people are sitting online, throwing statements at people and about topics, that they may not even have the right information about. 



